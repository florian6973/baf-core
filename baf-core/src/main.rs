use clap::Parser;
use std::{fs, os::windows::fs::MetadataExt};
use std::io::Read;
use md5::{Digest, Md5};
use file_id::{self};

use std::path::{PathBuf};
use walkdir::WalkDir;
use std::time::{SystemTime};
use serde::Serialize;
use serde::ser::SerializeStruct;



#[derive(Parser)]

struct Cli {
    command: String,
    path: std::path::PathBuf,
}

struct FileInfo {
    path: PathBuf,
    md5: String,
    mime_type: String,
    modified: SystemTime,
    created: SystemTime,
    size: u64,
    inode: String
}

impl Serialize for FileInfo {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where S: serde::Serializer {
        let mut state = serializer.serialize_struct("FileInfo", 7)?;
        state.serialize_field("path", &self.path)?;
        state.serialize_field("md5", &self.md5)?;
        state.serialize_field("mime_type", &self.mime_type)?;
        state.serialize_field("modified", &self.modified)?;
        state.serialize_field("created", &self.created)?;
        state.serialize_field("size", &self.size)?;
        state.serialize_field("inode", &self.inode)?;
        state.end()
    }
}

struct DirInfo {
    path: PathBuf,
    files: Vec<(bool, String)>,
    inode: String,
    md5: String
}

impl Serialize for DirInfo {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where S: serde::Serializer {
        let mut state = serializer.serialize_struct("DirInfo", 3)?;
        state.serialize_field("path", &self.path)?;
        state.serialize_field("files", &self.files)?;
        state.serialize_field("inode", &self.inode)?;
        state.serialize_field("md5", &self.md5)?;
        state.end()
    }
}


fn calculate_md5(data: &Vec<(bool, String)>) -> String {
    let mut hasher = Md5::new();

    for (flag, string_data) in data {
        // Convert the bool to a byte and add it to the hash
        let flag_byte = if *flag { 1u8 } else { 0u8 };
        hasher.update(&[flag_byte]);

        // Add the string data to the hash
        hasher.update(string_data.as_bytes());
    }

    // Finalize the hash and convert it to a hex string
    format!("{:x}", hasher.finalize())
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Cli::parse();


    println!("pattern: {:?}, path: {:?}\n", args.command, args.path);

    let paths = WalkDir::new(&args.path).follow_links(true);
    let mut entries: Vec<_> = paths.into_iter().filter_map(|e| e.ok()).collect();
    entries.sort_by_key(|e| e.path().to_owned());


    let mut all_files:Vec<FileInfo> = Vec::new();
    let mut all_dirs:Vec<DirInfo> = Vec::new();
    let mut all_dirs_hm:std::collections::HashMap<String, Vec<(bool, String)>> = std::collections::HashMap::new();

    // https://github.com/robo9k/rust-magic-sys
    // https://crates.io/crates/magic
    for path in entries.into_iter().rev() {
        let pathbuf: std::path::PathBuf = path.path().to_path_buf();    
        println!("Name: {}", pathbuf.display());

        // if ends with .baf, skip
        if pathbuf.ends_with(".baf") {
            println!("Skipping .baf folder");
            println!("");
            continue;
        }

        let file_id = file_id::get_file_id(&pathbuf).unwrap();
        println!("File ID: {file_id:?}");

        let file_metadata = fs::metadata(&pathbuf)?;
        println!("Filetype: {:?}", file_metadata.file_type());
        println!("Modified: {:?}", file_metadata.modified());
        println!("Created: {:?}", file_metadata.created());
        println!("Is file: {:?}", file_metadata.is_file());

        let key: String;
        let parent_folder = &pathbuf.parent().unwrap().to_str().unwrap();

        if file_metadata.is_file() {
            let file_size = file_metadata.file_size();
            println!("File size: {:?}", file_size);
            let mut file = fs::File::open(&pathbuf)?;

            let mut hasher = Md5::new();

            let mut buffer = [0; 1024];
            loop {
                let n = file.read(&mut buffer)?;
                if n == 0 {
                    break;
                }
                hasher.update(&buffer[..n]);
            }

            let result = hasher.finalize();
            println!("MD5: {:x}", result);

            let flags = magic::cookie::Flags::MIME_TYPE | magic::cookie::Flags::MIME_ENCODING;
            let cookie = magic::Cookie::open(flags)?;
            let database = Default::default();
            let cookie = cookie.load(&database)?;
            


            let filetype = cookie.file(&pathbuf)?;
            println!("Filetype: {}", filetype);

            let fileinfo = FileInfo {
                path: pathbuf.clone(),
                md5: format!("{:x}", result),
                mime_type: filetype,
                modified: file_metadata.modified()?,
                created: file_metadata.created()?,
                size: file_size,
                inode: format!("{:?}", file_id)
            };
            all_files.push(fileinfo);
            key = format!("{:x}", result);
        }
        else {
            let data_value = all_dirs_hm.entry((&pathbuf.to_str().unwrap()).to_string()).or_insert(Vec::new());
            key = calculate_md5(&data_value);
            let dirinfo = DirInfo {
                path: pathbuf.clone(),
                files: data_value.clone(),
                inode: format!("{:?}", format!("{:?}", file_id)),
                md5: key.clone()
            };
            all_dirs.push(dirinfo);

        }

        all_dirs_hm.entry(parent_folder.to_string()).or_insert(Vec::new()).push((file_metadata.is_file(), key));

        println!("");
        // ou utiliser mimeguess
        // measure performance time 
    }
    let folder_path = args.path.join(".baf");
    fs::create_dir_all(&folder_path)?;

    let yaml_content = serde_yaml::to_string(&all_files)?;

    // Replace "output.yaml" with the desired output file path
    std::fs::write(&folder_path.join("files.yaml"), yaml_content)?;

    let yaml_content = serde_yaml::to_string(&all_dirs)?;

    // Replace "output.yaml" with the desired output file path
    std::fs::write(&folder_path.join("dirs.yaml"), yaml_content)?;


    Ok(())
}
